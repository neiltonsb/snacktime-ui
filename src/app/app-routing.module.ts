import { Routes, RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';

const routes: Routes = [
  {
    path: 'perfil',
    loadChildren: 'app/main/perfil/perfil.module#PerfilModule'
  },
  {
    path: '**',
    redirectTo: 'perfil'
  }
];

@NgModule({
  declarations: [],
  imports: [
    RouterModule.forRoot(routes)
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
