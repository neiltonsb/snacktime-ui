import { Vinculado } from './../core/model';
import { Injectable } from '@angular/core';

import { environment } from 'environments/environment';

import { SnackHttp } from './../security/snack-http';
import { ResumoVinculado } from '../core/model';

import * as moment from 'moment';

@Injectable({
  providedIn: 'root'
})
export class VinculadoService {

  vinculadosUrl: string;

  constructor(
    private http: SnackHttp
  ) { 
    this.vinculadosUrl = `${environment.apiUrl}/vinculados`;
  }

  listarResumido(): Promise<ResumoVinculado> {
    return this.http.get<ResumoVinculado>(`${this.vinculadosUrl}?resumo`)
      .toPromise()
      .then(response => {
        return response;
      });
  }

  buscarPeloId(id: number): Promise<Vinculado> {
    return this.http.get<Vinculado>(`${this.vinculadosUrl}/${id}`)
      .toPromise()
      .then(response => {
        const vinculado = response;

        this.converterStringsParaDatas([vinculado]);

        return vinculado;
      });
  }

  atualizar(vinculado: Vinculado): Promise<Vinculado> {
    return this.http.put<Vinculado>(`${this.vinculadosUrl}/${vinculado.id}`, vinculado)
      .toPromise();
  }

  private converterStringsParaDatas(vinculados: Vinculado[]) {
    for (const vinculado of vinculados) {
      vinculado.dataNascimento = moment(vinculado.dataNascimento,
        'YYYY-MM-DD').toDate();
    }
  }
}
