import { LogoutService } from './logout.service';
import { MatButtonModule, MatFormFieldModule, MatIconModule, MatInputModule } from '@angular/material';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { NgxMaskModule } from 'ngx-mask';

import { FuseSharedModule } from '@fuse/shared.module';

import { LoginFormComponent } from './login-form/login-form.component';
import { SecurityRoutingModule } from './security-routing.module';

import { JwtModule } from '@auth0/angular-jwt';
import { environment } from 'environments/environment';

export function tokenGetter() {
  return localStorage.getItem('token');
}

@NgModule({
  declarations: [LoginFormComponent],
  imports: [
    CommonModule,

    MatButtonModule,
    MatFormFieldModule,
    MatIconModule,
    MatInputModule,

    FuseSharedModule,

    JwtModule.forRoot({
      config: {
        tokenGetter: tokenGetter,
        whitelistedDomains: environment.tokenWhitelistedDomains,
        blacklistedRoutes: environment.tokenBlacklistedRoutes
      }
    }),

    NgxMaskModule.forRoot(),

    SecurityRoutingModule
  ],
  providers: [
    LogoutService
  ]
})
export class SecurityModule { }
