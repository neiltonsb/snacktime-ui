import { environment } from 'environments/environment';
import { Injectable } from '@angular/core';

import { AuthService } from './auth.service';
import { SnackHttp } from './snack-http';

@Injectable()
export class LogoutService {

  tokensRevokeUrl: string;

  constructor(
    private http: SnackHttp,
    private auth: AuthService
  ) {
    this.tokensRevokeUrl = `${environment.apiUrl}/tokens/revoke`;
  }

  logout() {
    return this.http.delete(this.tokensRevokeUrl, { withCredentials: true })
      .toPromise()
      .then(() => {
        this.auth.limparAccessToken();
      });
  }

}