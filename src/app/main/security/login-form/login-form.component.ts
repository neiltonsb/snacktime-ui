import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { Title } from '@angular/platform-browser';

import { fuseAnimations } from '@fuse/animations/index';
import { FuseConfigService } from '@fuse/services/config.service';

import { AuthService } from './../auth.service';
@Component({
  selector: 'app-login-form',
  templateUrl: './login-form.component.html',
  styleUrls: ['./login-form.component.scss'],
  encapsulation: ViewEncapsulation.None,
  animations   : fuseAnimations
})
export class LoginFormComponent implements OnInit {

  loginForm: FormGroup;

  /**
   * Constructor
   *
   * @param {FuseConfigService} _fuseConfigService
   * @param {FormBuilder} _formBuilder
   */
  constructor(
    private _fuseConfigService: FuseConfigService,
    private _formBuilder: FormBuilder,
    private auth: AuthService,
    private router: Router,
    private title: Title
  ) { 
    // Configure the layout
    this._fuseConfigService.config = {
      layout: {
          navbar   : {
              hidden: true
          },
          toolbar  : {
              hidden: true
          },
          footer   : {
              hidden: true
          },
          sidepanel: {
              hidden: true
          }
      }
    };
  }

  ngOnInit() {
    this.auth.limparAccessToken();
    this.title.setTitle('Snacktime - Login');

    this.loginForm = this._formBuilder.group({
      celular: ['', [Validators.required]]
    });
  }

  logar() {
    let celular = this.loginForm.get('celular').value as string;
    celular = celular.replace('(', '').replace(')', '').replace('-', '');

    this.auth.login(celular)
      .then(() => {
        this.router.navigate(['/perfil/vinculados']);
      })
      .catch(response => {
        if(typeof(response) === 'string') {
          this.loginForm.get('celular').setErrors({ 'celular_inexistente': true });
        }

        // chamar serviço de erro
      })
  }
}
