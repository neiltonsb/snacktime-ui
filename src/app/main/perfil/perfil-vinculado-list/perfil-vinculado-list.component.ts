import { FuseConfigService } from '@fuse/services/config.service';
import { Router } from '@angular/router';

import { Component, OnInit, ViewEncapsulation } from '@angular/core';

import { fuseAnimations } from '@fuse/animations';

import { ErrorHandlerService } from './../../core/error-handler.service';
import { VinculadoService } from './../../services/vinculado.service';
import { Title } from '@angular/platform-browser';

@Component({
  selector: 'app-perfil-vinculado-list',
  templateUrl: './perfil-vinculado-list.component.html',
  styleUrls: ['./perfil-vinculado-list.component.scss'],
  encapsulation: ViewEncapsulation.None,
  animations   : fuseAnimations
})
export class PerfilVinculadoListComponent implements OnInit {


  vinculados = [];
  displayedColumns = ['checkbox', 'nome', 'celular', 'statusCartao', 'buttons'];
  checkboxes: {};


  /**
     * Constructor
     *
     * @param {FuseConfigService} _fuseConfigService
     */
  constructor(
    private _fuseConfigService: FuseConfigService,
    private errorHandler: ErrorHandlerService,
    private vinculadoService: VinculadoService,
    private router: Router,
    private title: Title
  ) { 
    // Configure the layout
    this._fuseConfigService.config = {
      layout: {
          navbar   : {
              hidden: true
          },
          toolbar  : {
              hidden: true
          },
          footer   : {
              hidden: true
          },
          sidepanel: {
              hidden: true
          }
      }
    };
  }

  ngOnInit() {
    this.title.setTitle('SnackTime - Vinculados')
    this.vinculadoService.listarResumido()
      .then((response: any) => {
        this.vinculados = response;
      })
      .catch(error => this.errorHandler.handle(error));
  }

  editar(id: number) {
    this.router.navigate(['/perfil/vinculados', id])
  }

  alterarStatusCartao(vinculado: any) {
    console.log(vinculado);
  }

}
