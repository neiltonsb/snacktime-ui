import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatIconModule, MatButtonModule, MatRippleModule, MatTableModule, MatMenuModule, MatCheckbox, MatCheckboxModule, MatTooltipModule, MatFormFieldModule, MatSelectModule, MatDatepickerModule, MatInputModule } from '@angular/material';

import { PerfilSidebarComponent } from './perfil-sidebar/perfil-sidebar.component';
import { PerfilVinculadoListComponent } from './perfil-vinculado-list/perfil-vinculado-list.component';
import { PerfilComponent } from './perfil.component';

import { FuseSidebarModule } from '@fuse/components/sidebar/sidebar.module';
import { PerfilVinculadoFormComponent } from './perfil-vinculado-form/perfil-vinculado-form.component';

import { NgxPhoneMaskBrModule } from 'ngx-phone-mask-br';

import { PerfilRoutingModule } from './perfil-routing.module';
import { FuseSharedModule } from './../../../@fuse/shared.module';

@NgModule({
  declarations: [
    PerfilComponent,
    PerfilSidebarComponent,
    PerfilVinculadoListComponent,
    PerfilVinculadoFormComponent
  ],
  imports: [
    CommonModule,

    MatIconModule,
    MatButtonModule,
    MatRippleModule,
    MatTableModule,
    MatMenuModule,
    MatCheckboxModule,
    MatTooltipModule,
    MatFormFieldModule,
    MatSelectModule,
    MatDatepickerModule,
    MatInputModule,

    FuseSharedModule,
    FuseSidebarModule,

    NgxPhoneMaskBrModule,

    PerfilRoutingModule
  ]
})
export class PerfilModule { }
