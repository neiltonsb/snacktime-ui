import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PerfilComponent } from './perfil.component';
import { PerfilVinculadoFormComponent } from './perfil-vinculado-form/perfil-vinculado-form.component';
import { PerfilVinculadoListComponent } from './perfil-vinculado-list/perfil-vinculado-list.component';

const routes: Routes = [
  { 
    path: '',
    component: PerfilComponent,
    children: [
      {
        path: 'vinculados',
        component: PerfilVinculadoListComponent
      },
      {
        path: 'vinculados/:id',
        component: PerfilVinculadoFormComponent
      }
    ]
  }
]

@NgModule({
  declarations: [],
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [RouterModule]
})
export class PerfilRoutingModule { }
