import { AuthService } from './../../security/auth.service';
import { Vinculado } from './../../core/model';
import { ToastrService } from 'ngx-toastr';
import { ErrorHandlerService } from './../../core/error-handler.service';
import { FuseConfigService } from '@fuse/services/config.service';
import { MomentDateAdapter } from '@angular/material-moment-adapter';
import { ActivatedRoute } from '@angular/router';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Component, OnInit, ViewEncapsulation } from '@angular/core';

import { DateAdapter, MAT_DATE_LOCALE, MAT_DATE_FORMATS } from '@angular/material';

import { fuseAnimations } from '@fuse/animations/index';

import { VinculadoService } from './../../services/vinculado.service';
import { Title } from '@angular/platform-browser';
import { errorHandler } from '@angular/platform-browser/src/browser';

export const MY_FORMATS = {
  parse: {
    dateInput: 'DD/MM/YYYY',
  },
  display: {
    dateInput: 'DD/MM/YYYY',
    monthYearLabel: 'MMM YYYY',
    dateA11yLabel: 'LL',
    monthYearA11yLabel: 'MMMM YYYY',
  },
};

@Component({
  selector: 'app-perfil-vinculado-form',
  templateUrl: './perfil-vinculado-form.component.html',
  styleUrls: ['./perfil-vinculado-form.component.scss'],
  encapsulation: ViewEncapsulation.None,
  animations   : fuseAnimations,
  providers: [
    { provide: MAT_DATE_LOCALE, useValue: 'pt' },

    { provide: DateAdapter, useClass: MomentDateAdapter, deps: [MAT_DATE_LOCALE] },

    { provide: MAT_DATE_FORMATS, useValue: MY_FORMATS },
  ]
})
export class PerfilVinculadoFormComponent implements OnInit {

  vinculado = new Vinculado();
  generos = [
    { value: 'M', viewValue: 'Masculino' },
    { value: 'F', viewValue: 'Feminino' }
  ];

  /**
   * Constructor
   * 
   * @param {FuseConfigService} _fuseConfigService
   */
  constructor(
    private _fuseConfigService: FuseConfigService,
    private route: ActivatedRoute,
    private vinculadoService: VinculadoService,
    private title: Title,
    private errorHandler: ErrorHandlerService,
    private toastr: ToastrService,
    private auth: AuthService
  ) { 
    // Configure the layout
    this._fuseConfigService.config = {
      layout: {
          navbar   : {
              hidden: true
          },
          toolbar  : {
              hidden: true
          },
          footer   : {
              hidden: true
          },
          sidepanel: {
              hidden: true
          }
      }
    };
  }

  ngOnInit() {
    const idVinculado = this.route.snapshot.params['id'];

    this.title.setTitle('SnackTime - Novo vinculado');
    
    if(idVinculado) {
      this.carregarVinculado(idVinculado);
      
    }
  }

  carregarVinculado(id: number) {
    this.vinculadoService.buscarPeloId(id)
    .then(vinculado => {
      this.vinculado = vinculado;

      this.atualizarTituloEdicao();
    })
    .catch(error => this.errorHandler.handle(error));
  }

  salvar() {
    if(this.editando) {
      this.atualizarVinculado();
    };
  }

  atualizarVinculado() {
    this.vinculadoService.atualizar(this.vinculado)
      .then(vinculado => {
        this.vinculado = vinculado;

        this.toastr.success('Vinculado atualizado com sucesso!', 'OK');

        this.atualizarTituloEdicao();
      })
      .catch(error => this.errorHandler.handle(error));
  }

  atualizarTituloEdicao() {
    this.title.setTitle(`Snacktime - Editando ${this.vinculado.nome}`);
  }

  get editando() {
    return Boolean(this.vinculado.id);
  }

}
