import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';

import { LogoutService } from './../../security/logout.service';
import { AuthService } from './../../security/auth.service';

@Component({
  selector: 'app-perfil-sidebar',
  templateUrl: './perfil-sidebar.component.html',
  styleUrls: ['./perfil-sidebar.component.scss']
})
export class PerfilSidebarComponent implements OnInit {

  constructor(
    public auth: AuthService,
    private logoutService: LogoutService,
    private router: Router
  ) { }

  ngOnInit() {
  }

  logout() {
    this.logoutService.logout()
      .then(() => {
        this.router.navigate(['/login']);
      })
      .catch(error => {
        console.error('Erro ao fazer logou', error);
      });
  }

}
