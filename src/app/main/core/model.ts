export class ResumoVinculado {
    id: number;
    nome: string;
    telefone: string;
    cartao: Cartao;
}

export class Vinculado {
    id: number;
    nome: string;
    genero: string;
    dataNascimento: Date;
    telefone: string;
    responsavel = new Responsavel();
    cartao: Cartao;
    estabelecimento: Estabelecimento;
}

export class Responsavel {
    id: number;
    nome: string;
    email: string;
    telefone: string;
    cartao: Cartao;
}

export class Cartao {
    id: number;
    codigo: string;
    ilimitado: boolean;
    limiteDiario: number;
    ativo: boolean;
}

export class Estabelecimento {
    id: number;
    tipoPessoa: string;
    nomeRazaoSocial: string;
    cpfCnpj: string;
    inscricaoEstadual: string;
    telefone: string;
    email: string;
    ativo: boolean;
    logradouro: string;
    numero: number;
    complemento: string;
    bairro: string;
    cidade: string;
    estado: string;
    cep: string;
}