import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { Title } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';


import { ToastrModule } from 'ngx-toastr';

import { JwtHelperService } from '@auth0/angular-jwt';

import { VinculadoService } from './../services/vinculado.service';
import { AuthService } from './../security/auth.service';
import { SnackHttp } from './../security/snack-http';
import { ErrorHandlerService } from './error-handler.service';


@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    HttpClientModule,
    RouterModule,
    BrowserAnimationsModule,
    
    ToastrModule.forRoot() 
  ],
  providers: [
    VinculadoService,
    ErrorHandlerService,
    AuthService,
    SnackHttp,

    JwtHelperService,

    Title
  ]
})
export class CoreModule { }
